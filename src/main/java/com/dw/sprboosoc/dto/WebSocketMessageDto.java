package com.dw.sprboosoc.dto;

import com.dw.sprboosoc.constant.MessageEnum;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * WebSocket 消息 dto
 * dingwen
 * 2021/2/20 14:31
 **/
@Getter
@Setter
@ToString
public class WebSocketMessageDto implements Serializable {
    private static final long serialVersionUID = 4153093005674764992L;
     /*发信人*/
    private String sendUserId;

    /*收信人*/
    private String recvUserId;

    /*消息内容*/
    private String message;

    /*消息类型*/
    private MessageEnum messageEnum;
}
