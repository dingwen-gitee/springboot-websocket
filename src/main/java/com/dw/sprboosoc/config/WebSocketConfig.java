package com.dw.sprboosoc.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.server.standard.ServerEndpointExporter;

/**
 * WebSocket 配置类
 * dingwen
 * 2021/2/20 10:37
 **/
@Configuration
public class WebSocketConfig {

    /*
     * ServerEndpointExporter 会自动注册使用@ServerEndpoint注解声明的websocket endpoint
     * @param []
     * @return org.springframework.web.socket.server.standard.ServerEndpointExporter
     */
    @Bean
    public ServerEndpointExporter serverEndpointExporter(){
        return new ServerEndpointExporter();
    }
}
