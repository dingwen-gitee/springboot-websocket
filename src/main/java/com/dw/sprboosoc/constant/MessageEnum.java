package com.dw.sprboosoc.constant;

import lombok.Getter;

import java.util.Arrays;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * 消息枚举
 *
 * @author diingwen
 * @date 2021/08/27
 */
@Getter
public enum MessageEnum {

    /**
     * 私发
     */
    ONE("one", "私发"),

    /**
     * 群发
     */
    ALL("all", "群发"),

    /**
     * 其他
     */
    OTHER("other", "其他");


    /**
     * 消息类型
     */
    private final String messageType;


    /**
     * 消息类型描述
     */
    private final String desc;

    /**
     * 没有找到枚举类型异常提示
     */
    private static final String EXCEPTION_NOT_FIND_TIP = "没有找到消息类型为{}的枚举，请检查方法入参";
    private static final String EXCEPTION_MESSAGE_type_TIP = "消息类型（方法入参）不能为空";

    /**
     * 消息枚举 构造器 私有化
     *
     * @param messageType 消息类型
     * @param desc        desc
     */
    MessageEnum(String messageType, String desc) {
        this.messageType = messageType;
        this.desc = desc;
    }


    /**
     * 获取消息类型
     *
     * @param messageType 消息类型
     * @return {@link MessageEnum}
     */
    public static MessageEnum getMessageEnum(String messageType) {
        Optional.ofNullable(messageType).orElseThrow(() -> new IllegalArgumentException(EXCEPTION_MESSAGE_type_TIP));
        try {
            return Arrays.stream(MessageEnum.values())
                    .parallel()
                    .filter(messageEnum -> messageEnum.getMessageType().equals(messageType.toLowerCase()))
                    .limit(1)
                    .collect(Collectors.toList())
                    .get(0);
        } catch (Exception e) {
            throw new IllegalArgumentException(EXCEPTION_NOT_FIND_TIP.replace("{}",messageType));
        }
    }

}

